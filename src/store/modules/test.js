export default {
  state: {
    TEST: []
  },
  getters: {
    GET_TEST(state) {
      return state.TEST
    },
  },

  actions: {
    FETCH_TEST({ commit }) {
      // https://opentdb.com/api.php?amount=10
      fetch(`https://opentdb.com/api.php?amount=10`)
        .then(resp => resp.json())
        .then(json => {
          commit('UPDATE_TEST_ANSWERS', json.results)
        })
    }
  },
  mutations: {
    UPDATE_TEST_ANSWERS(state, data) {
      data.forEach(el => {
        let tmpAnsw = [...el.incorrect_answers]
        tmpAnsw.push(el.correct_answer)
        el.answers = [...tmpAnsw].sort(() => Math.random() - 0.5)
      })
      state.TEST = data
    },
    UPDATE_QUESTION(state, data, index) {
      console.log('UPDATE_QUESTION', data.question)
      state.TEST[index] = data
    }
  },
}