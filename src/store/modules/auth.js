export default {
  state: {
    token: null
  },
  getters: {
    GET_TOKEN: state => {
      return state.token
    },
  },

  mutations: {
    SET_TOKEN: (state, token) => state.token = token,
  },
  actions: {
    SET_TOKEN({ commit }) {
      let token = !localStorage.getItem('r2d2') 
        ? function() {
          console.error('token is failed..')
          const id = new Date().valueOf()
          localStorage.setItem('r2d2', id)
          console.error('session: id is', id)
          return id
        }()
        : localStorage.getItem('r2d2')
  
      commit('SET_TOKEN', token)
    },
  },
}