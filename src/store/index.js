import { createStore } from 'vuex'
import auth from './modules/auth'
import test from './modules/test'

export default createStore({
  modules: {
    auth,
    test
  }
})
